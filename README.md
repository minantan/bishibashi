# BishiBashi

BishiBashi is a Corona SDK port of Konami's Hyper Bishi Bashi, Karate Master level

To get started:

* Obtain Corona SDK - http://www.coronalabs.com/products/corona-sdk/
* Sign up for a free license
* Clone the repository, open the `main.lua` file in the simulator
* Generate an APK or XCode simulator file, more information at:
    * http://developer.coronalabs.com/content/building-devices-iphoneipad (iOS)
    * http://developer.coronalabs.com/content/building-devices-android (APK)

## Features

* 2p over a network
* 1p easy, medium, hard, high scores and how to play

## Issues

* If using WiFi tethering, the WiFi provider needs to be the client. Didn't have time to find out why :/
* Only one game can be played on the network. I've set the client to auto connect to the first available server :)
* The random seed is not so random
* Various exciting bugs and clipping/alignment issues

