local storyboard = require( "storyboard" )
local config = require( "scripts.karate.config" )
local scene = storyboard.newScene()
local utils = require("scripts.utils")
local text = require("scripts.text")
local assets = require("scripts.assets")
local widget = require("scripts.customWidget")

local asset = assets.create()

function scene:createScene( event )	
	local group = self.view
	asset:loadSound("selected", "sounds/selected.wav")
	asset:loadSound("loop", "sounds/loop.wav")

	local background = display.newImageRect( "logo.png", display.contentWidth, display.contentHeight + 100 )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, -50

	local buttonGroup = asset:loadDisplay("buttons", display.newGroup())

	local buttonWidth, buttonHeight, buttonRadius = display.contentWidth - 120, 40, 10

	local singlePlayer = widget.newButton(0, 310, buttonWidth, buttonHeight, buttonRadius, { fill = {0,255,0,150} }, "Single Player")
	utils.center(singlePlayer)
	local client = widget.newButton(0, 360, buttonWidth, buttonHeight, buttonRadius, { fill = {255,0,0,150} }, "Start Client")
	utils.center(client)
	local server = widget.newButton(0, 410, buttonWidth, buttonHeight, buttonRadius, { fill = {255,255,0,150} }, "Start Server")
	utils.center(server)
	local highScore = widget.newButton(0, 460, buttonWidth, buttonHeight, buttonRadius, { fill = {0,0,255,150} }, "High Scores")
	utils.center(highScore)

	buttonGroup:insert( highScore )
	buttonGroup:insert( client )
	buttonGroup:insert( server )
	buttonGroup:insert( singlePlayer )

	group:insert( background )
	group:insert(buttonGroup)

	singlePlayer:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "difficulty", { effect = "flip", time = 300 })
			end
		})				
		return true	
	end)
	highScore:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.highScore", { effect = "flip", time = 300 })
			end
		})
		return true	

	end)
	client:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.karateScene", { effect = "fade", time = 500, params = { start = "client", difficulty = "normal" } })
			end
		})
		return true	

	end)
	server:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.karateScene", { effect = "fade", time = 500, params = { start = "server", difficulty = "normal" } })
			end
		})		
		return true	
	end)
end

function scene:key( event )
	return utils.onKeyEvent(event, storyboard.returnTo)
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view	
	storyboard.returnTo = nil
  utils.playBG(asset.sounds["loop"], { loops = -1 })
	asset:addListener("key", scene)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	-- audio.fadeOut({ channel = 1, time = 500})
	asset:disposeTemps()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	asset:dispose()
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene