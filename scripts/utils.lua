local storyboard = require("storyboard")

local center = function(obj, options)
  local width = display.contentWidth
  -- is it a display group? Reference points for groups are top left
  if (obj.numChildren) then
    obj.x = (width - obj.contentWidth) * 0.5
  else
    obj.x = (width * 0.5)
  end
  if (options) then
    if (options.x) then
      obj.x = obj.x + options.x
    end
    if (options.y) then
      obj.y = obj.y + options.y
    end
  end

end

local undoScale = function(obj, options)
  if (options.x) then  
    obj.width = obj.width / options.x
    obj.x = obj.x / options.x
  end
  if (options.y) then
    obj.height = obj.height / options.y
    obj.y = obj.y / options.y
  end
end

local play = function(handle, channel, options)
  local options = options or {}
  if (channel) then
    options.channel = channel
  else
    options.channel = 2
  end
  audio.play(handle, options)
end

local playFX = function(handle, options)
  play(handle, 2, options)
end

local playBG = function(handle, options)
  play(handle, 1, options)
end

local stopFX = function()
  audio.stop(2)
end

local stopBG = function()
  audio.stop(1)
end

-- Taken from http://www.coronalabs.com/blog/2013/03/26/androidizing-your-mobile-app/
local function onKeyEvent( event, lastScene )

  local phase = event.phase
  local keyName = event.keyName

  if ( "back" == keyName and phase == "up" ) then
    -- print( "previous scene", lastScene )
    if ( lastScene ) then
      storyboard.gotoScene( lastScene, { effect="flip", time=300 } )
    else
      native.requestExit()
    end
  end
  if ("back" == keyName) then
    return true
  end

end

return {
  center = center,
  undoScale = undoScale,
  playFX = playFX,
  playBG = playBG,
  play = play,
  stopFX = stopFX,
  stopBG = stopBG,
  onKeyEvent = onKeyEvent
}

