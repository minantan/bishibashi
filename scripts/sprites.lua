local createSprite = function(options) 
  return {
    options
  }
end

local createSpriteSequence = function(options)
  local seq = {}
  for i = 1, options.total do
    seq[i] = {
      x = options.startX,
      y = options.startY + (options.height * (i - 1)),
      width = options.width,
      height = options.height
    }
  end 
  return seq
end

local constructFrameOptions = function(framePairs)
  local frameOptions = { }
  
  local frame = 0
  for k,v in pairs(framePairs) do

    v.start = frame + 1
    local frames = {}
    
    if (v.total) then
      frames = createSpriteSequence(v)
    else
      frames = createSprite(v)
    end
    
    for i = 1, #frames do
      frame = frame + 1
      frameOptions[frame] = frames[i]
    end
  end

  return { frames = frameOptions }
end

local makeSequence = function(frames, name, spriteName, time, loopCount)
  return {
    name = name,
    start = frames[spriteName].start,
    count = frames[spriteName].total,
    time = time,
    loopCount = loopCount
  }
end

return {
  
  constructFrameOptions = constructFrameOptions,
  makeSequence = makeSequence

}