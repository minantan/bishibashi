local text = require("scripts.text")

local defaults = {
  text = {
    fill = graphics.newGradient({255, 165, 0}, {255, 255, 255}),
    font = "Impact"
  }
}

local newButton = function(x, y, width, height, rad, buttonOpts, label, labelOpts)
  local buttonOpts = buttonOpts or {}
  local labelOpts = labelOpts or {
    font = defaults.text.font,
    fill = defaults.text.fill,
    size = height - 8
  }

  local buttonGroup = display.newGroup()
  local button = display.newRoundedRect(x, y, width, height, rad)
  if (buttonOpts.fill) then
    button:setFillColor(buttonOpts.fill[1], buttonOpts.fill[2], buttonOpts.fill[3], buttonOpts.fill[4])
  end


  local buttonText = text.createText(label, labelOpts.font, labelOpts.size, labelOpts.fill, 0, 0)
  local textWidth, textHeight = buttonText.text.contentWidth, buttonText.text.contentHeight

  local textX, textY = button.x - (textWidth * 0.5), button.y - (textHeight) * 0.5
  buttonText.group.x, buttonText.group.y = textX, textY

  buttonGroup:insert(button)
  buttonGroup:insert(buttonText.group)

  return buttonGroup
end

return {
  newButton = newButton
}