local text = require("scripts.text")

local createStatus = function(config, heading, y, maxScore)

  -- todo remove
  local screenW, halfW, screenH = display.contentWidth, display.contentWidth *0.5, display.contentHeight

  local statusGroup = display.newGroup()

  local status = display.newImageRect('images/status.png', screenW, screenW / 4.25)  
  status.x, status.y = halfW, y

  local completionBar = display.newRect(halfW + (9 / 160 * halfW), status.y - 35, halfW - (13 / 160 * halfW), 25)
  completionBar:setFillColor(255, 255, 0)

  local completionBarMaxWidth = completionBar.contentWidth
  completionBar.x = completionBar.x - completionBarMaxWidth

  local completionBarStart = completionBar.x

  local statusBacking = display.newRect(0, 0, status.contentWidth, status.contentHeight)
  statusBacking.x, statusBacking.y = status.x, status.y
  statusBacking:setFillColor(0,0,0)
  local statusText = text.createText(heading, config.text.font, 45, config.text.fill, 10, status.y - 35)

  local score = config.game.initialScore
  local increment = completionBarMaxWidth / maxScore

  local scoreText = text.createText(score, config.text.font, 35, config.text.fill, halfW + (12 / 16 * halfW) , y - 13)

  local bar = display.newGroup()
  bar:insert(statusBacking)
  bar:insert(completionBar)
  bar:insert(status)

  local texts = display.newGroup()
  texts:insert(statusText.group)
  texts:insert(scoreText.group)

  statusGroup:insert( bar )
  statusGroup:insert( texts )

  return {
    group = statusGroup,
    texts = texts,
    bar = bar,
    setScore = function(self, newScore)
      score = newScore
      scoreText.text.text, scoreText.shadow.text = newScore, newScore
      completionBar.x = completionBarStart + (increment * newScore)
    end,
    getScore = function(self)
      return score
    end
  }
end

return {
  createStatus = createStatus
} 