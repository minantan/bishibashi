local utils = require("scripts.utils")

local shadowOffset = 2
local createText = function(text, font, size, color, x, y)
    
  local textGroup = display.newGroup()

  local textObject = display.newText(text, 0, 0, font, size)
  textObject:setTextColor(color)

  local textShadow = display.newText(textObject.text, x, y, font, size)
  textShadow.x, textShadow.y = textObject.x + shadowOffset, textObject.y + shadowOffset
  textShadow:setTextColor({0,0,0})

  textGroup:insert(textShadow)
  textGroup:insert(textObject)
  textGroup.x, textGroup.y = x, y

  return {
    group = textGroup, 
    text = textObject, 
    shadow = textShadow,
    setText = function(self, newText)
      textObject.text = newText
      textShadow.text = newText
    end,
    removeSelf = function(self)    
      textGroup:removeSelf()
    end,
    center = function(self)
      utils.center(textGroup)
    end
  }
end

return {
  createText = createText
}