local sprites = require("scripts.karate.sprites")
local config = require("scripts.karate.config")
local halfW, halfH = display.contentWidth * 0.5, display.contentHeight * 0.5

return {

  generateBadGuys = function()  
    require("scripts.lib.randomlua")
    local random = mwc(config.game.randomseed)
    local badGuysStopped = false
    local badGuys = {}  
    
    local stopBadGuys = function()
      badGuysStopped = true
      for i = 1, #badGuys do
        badGuys[i]:pause()
      end
    end

    local makeBadGuy = function(x, y, scaleX, scaleY, badGuyType) 
      local badGuySprite = display.newSprite(sprites.sheet, {
        sprites.makeSequence("advancing", "badGuyLeft", 400, 0),
        sprites.makeSequence("hit", "badGuyHitLeft", 400, 0),
        sprites.makeSequence("hitCentre", "badGuyHitCentre", 400, 0)
      })

      badGuySprite.x, badGuySprite.y = x, y
      badGuySprite.xScale, badGuySprite.yScale = scaleX, scaleY
      
      return {
        advance = function(self, y, time)
          badGuySprite:play()
          badGuySprite.trans = transition.to(badGuySprite, {
            time = time,
            y = y
          })
        end,
        gotHit = function(self)

          for i = 1, #badGuys do
            if (badGuys[i] == self) then
              table.remove(badGuys, i)
              break
            end
          end

          if (badGuySprite.trans ~= nil) then
            badGuySprite:dispatchEvent({ 
              name = "gotHit",
              target = badGuySprite 
            })

            transition.cancel(badGuySprite.trans)

            badGuySprite:setSequence("hit")
            local xDelta, yDelta = -20, 20
            if (badGuyType == "right") then
              xDelta = xDelta * (-1)
            elseif (badGuyType == "centre") then
              xDelta = 0
              badGuySprite:setSequence("hitCentre")
            end

            transition.to(badGuySprite, {
              time = 300,
              x = badGuySprite.x + xDelta,
              y = badGuySprite.y + yDelta,
              onComplete = function(e) 
                badGuySprite:removeSelf()
                badGuySprite = nil
              end
            })
          end
        end,
        pause = function(self)
          if (badGuySprite.trans ~= nil) then
            badGuySprite:pause()
            transition.cancel(badGuySprite.trans)       
          end
        end,
        addEventListener = function(self, eventName, listener)
          badGuySprite:addEventListener(eventName, listener)
        end,
        sprite = badGuySprite,
        badGuyType = badGuyType
      }
    end

    local generateBadGuy = function(xScale, yScale, fixedBadGuyType)

      local badGuyType = random:random(0, 3)
      if (fixedBadGuyType) then
        if (fixedBadGuyType == "left") then
          badGuyType = 0
        elseif (fixedBadGuyType == "centre") then
          badGuyType = 1
        elseif (fixedBadGuyType == "right") then
          badGuyType = 2
        end
      end
      local badGuy
      if (badGuyType == 0) then
        badGuy = makeBadGuy(halfW - (9 / 16 * halfW), halfH + 350, xScale, yScale, "left")
      elseif (badGuyType == 1) then
        badGuy = makeBadGuy(halfW, halfH + 350, xScale, yScale, "centre")   
      else
        badGuy = makeBadGuy(halfW + (9 / 16 * halfW), halfH + 350, -xScale, yScale, "right")
      end

      badGuys[#badGuys + 1] = badGuy
      
      return badGuy

    end

    return {
      generateBadGuy = generateBadGuy,
      stopBadGuys = stopBadGuys,
      clear = function()
        for j = 1, #badGuys do
          badGuys[j].sprite:removeSelf()
          badGuys[j].sprite = nil
        end
        badGuys = {}
      end,
      isStopped = function()
        return badGuysStopped
      end,
      startBadGuys = function()
        badGuysStopped = false
      end,
      badGuys = function()
        return badGuys
      end    

    }

  end
  
}