local config = require("scripts.karate.config")
local sprites = require("scripts.karate.sprites")
local text = require("scripts.text")
local status = require("scripts.status")
local goodGuy = require("scripts.karate.goodGuy")
local badGuys = require("scripts.karate.badGuy")
local utils = require("scripts.utils")

-- forward declarations and other locals
local screenW, screenH, halfW, halfH = 
	display.contentWidth,
	display.contentHeight, 
	display.contentWidth * 0.5, 
	display.contentHeight * 0.5

local start = function( group, maxScore, minTimeBetweenBaddies )
	require("scripts.lib.randomlua")
	local random = mwc(config.game.randomseed)

	local lastBadGuy = 0
	local finished = false
	local timeStart = system.getTimer()
	local statusBar = status.createStatus(config, "You", screenH - 25, maxScore)

	local timeBetweenBaddies = 700

	-- create a grey rectangle as the backdrop
	local background = display.newImageRect(sprites.sheet, sprites.frames["bg"].start, screenW, screenH + 50)
	background.x, background.y = halfW, halfH


	local yellow, red, green = display.newRect(0, 0, screenW / 3, screenW / 3),
		display.newRect(0, 0, screenW / 3, screenW / 3),
		display.newRect(0, 0, screenW / 3, screenW / 3)

  yellow:setFillColor(255, 255, 0, 80)
  red:setFillColor(255, 0, 0, 80)
  green:setFillColor(0, 255, 0, 80)

	yellow.x, red.x, green.x = screenW * 0.17, screenW * 0.5, screenW * 0.83
	yellow.y, red.y, green.y = halfH + 173, halfH + 173, halfH + 173

	-- bad guy
	local badGuy = badGuys.generateBadGuys()
	-- good guy
	local gg = goodGuy.makeGoodGuy(halfW, halfH - 150, 2, 2 * group.xScale, true)

	gg:idle()

  local k = display.newGroup()

	gg:addEventListener("hitBadGuy", function (e)
		for i = 1, #badGuy.badGuys() do
			local thisBadGuy = badGuy.badGuys()[i]
      if (thisBadGuy.sprite.y <= (e.target.y + 100)) then
        if (thisBadGuy.badGuyType == e.direction) then
          thisBadGuy:gotHit()
          statusBar:setScore(statusBar:getScore() + 1)
          k:dispatchEvent({ name = "score", score = statusBar:getScore() })
          timeBetweenBaddies = timeBetweenBaddies * minTimeBetweenBaddies
          return
        end
      end
    end
	end)

	red:addEventListener("touch", function(e)
		if (e.phase == "began" and not badGuy.isStopped()) then
			gg.punch()
			k:dispatchEvent({ name = "red" })
		end
	end)

	yellow:addEventListener("touch", function(e)
		if (e.phase == "began" and not badGuy.isStopped()) then
			gg.lKick()
			k:dispatchEvent({ name = "yellow" })
		end
	end)

	green:addEventListener("touch", function(e)
		if (e.phase == "began" and not badGuy.isStopped()) then
			gg.rPunch()
			k:dispatchEvent({ name = "green" })
		end
	end)	

	local buttonGroup = display.newGroup()
	local badGuyGroup = display.newGroup()
	group:insert( background )
	group:insert( gg.sprite )
	group:insert( badGuyGroup )

	buttonGroup:insert( yellow )
	buttonGroup:insert( red )
	buttonGroup:insert( green )
	buttonGroup:insert( statusBar.group )
	
	group:insert( buttonGroup )

	local screenFlash = display.newRect(0, 0, screenW, screenH + 50)
	screenFlash:setFillColor(255, 255, 255)
	screenFlash.alpha = 0
	screenFlash.x, screenFlash.y = halfW, halfH		
	group:insert (screenFlash)

	local mainEventLoop = function(e)
		if (finished) then
			return
		end		
		
		if (not badGuy.isStopped()) then
			local currentScore = statusBar:getScore()

			if ((random:random(0,5) == 0) and (e.time - lastBadGuy > timeBetweenBaddies))  then
				lastBadGuy = e.time
				local bg = badGuy.generateBadGuy(2, 2 * group.xScale)
				k:dispatchEvent({ name = "badGuy", badGuyType = bg.badGuyType})
				badGuyGroup:insert(bg.sprite)
				bg:advance(gg.sprite.y, config.game.delays.advance)
				bg:addEventListener("gotHit", function(e)
	        transition.to(screenFlash, {
	          alpha = 1,
	          time = 100,
	          onComplete = function ( e)    
	            transition.to(screenFlash, {
	              alpha = 0,
	              time = 100
	            })
	          end
	        })
				end)

			end

			for i = 1, #badGuy.badGuys() do
				local badGuyY = badGuy.badGuys()[i].sprite.y
  			if (badGuyY ~= nil and gg.sprite.y and (badGuyY <= gg.sprite.y + 50)) then
					badGuy.stopBadGuys()					
					gg:gotHit()
					timer.performWithDelay(config.game.delays.resetAfterHit, function(e)						
						badGuy.clear()
						gg:idle()
						badGuy.startBadGuys()
					end)
					break
				end
			end			
		end

	end
  Runtime:addEventListener("enterFrame", mainEventLoop)

  return {
  	red = function(self) 
	 		if (not badGuy.isStopped()) then
				gg:punch()
			end
	 	end,
		yellow = function(self)
			if (not badGuy.isStopped()) then
				gg:lKick()
			end
		end,
		green = function(self)
			if (not badGuy.isStopped()) then
				gg:rPunch()
			end
		end,
		gotHit = function(self)
			gg:gotHit()
		end,
		stop = function(self)
			badGuy.stopBadGuys()
			badGuy.clear()
			Runtime:removeEventListener("enterFrame", mainEventLoop)		
		end,
		score = function(self)
			return statusBar:getScore() 
		end,
		finished = function(self)
			return finished
		end,
		finish = function(self)
			finished = true
			self:stop()
			k:dispatchEvent({ name = "endScore", endScore = statusBar:getScore() })
		end,
		addEventListener = function(self, event, listener) 
			k:addEventListener(event, listener)			
		end
	}

end

return {
	start = start,
}