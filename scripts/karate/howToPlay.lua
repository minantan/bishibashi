local config = require("scripts.karate.config")
local storyboard = require( "storyboard" )
local text = require("scripts.text")
local utils = require("scripts.utils")
local assets = require("scripts.assets")
local scores = require("scripts.karate.scores")
local scene = storyboard.newScene()

--------------------------------------------
-- forward declarations and other locals

local screenW, screenH, halfW, halfH = 
  display.contentWidth, 
  display.contentHeight, 
  display.contentWidth * 0.5, 
  display.contentHeight * 0.5

local asset = assets.create()

-- Called when the scene's view does not exist:
function scene:createScene( event )
  local group = self.view

  -- preload sounds
  asset:loadSound("loop", "sounds/loop2.wav")
  asset:loadSound("end", "sounds/end.wav")
  asset:loadSound("explode", "sounds/explode.wav")
  asset:loadSound("cheering", "sounds/cheering.wav")
  asset:loadSound("fail", "sounds/fail.wav")
  
end

local leftGame

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  local group = self.view

  storyboard.returnTo = "difficulty"
  
  asset:addListener("key", scene)  

  local karatePlayer = require("scripts.karate.karatePlayer")
  
  local goToMenu = function()
    storyboard.gotoScene("menu", { effect = "flip", time = 300 })
  end

  local maxScore = config.game.maxScore

  local startGames = function() 
    system.setIdleTimer( false )

    local leftGroup = asset:loadDisplay("left", display.newGroup())

    group:insert(leftGroup)
    utils.stopBG()
    utils.playBG(asset.sounds["loop"], { loops = -1, fadeIn = 1000 })
    leftGame = karatePlayer.start(leftGroup, maxScore, { sound = true, buttons = true })

    local dialogGroup = asset:loadDisplay("dialog", display.newGroup())
    local dialogBg = display.newRoundedRect(0, halfH -20, display.contentWidth - 50, 120, 10)
    dialogBg:setFillColor(0,0,200,200)
    utils.center(dialogBg)
    local helpText = text.createText("Help Karate Master", config.text.font, 30, config.text.fill, halfW, halfH - 10)
    local helpText2 = text.createText("defeat the thugs!", config.text.font, 30, config.text.fill, halfW, halfH + 30)
    local helpText3 = text.createText("Touch to continue...", config.text.font, 15, config.text.fill, halfW, halfH + 70)
    utils.center(helpText.group)
    utils.center(helpText2.group)
    dialogGroup:insert(dialogBg)
    dialogGroup:insert(helpText.group)
    dialogGroup:insert(helpText2.group)
    dialogGroup:insert(helpText3.group)

    local page = 1

    local hit = function(e)
      page = 4
      helpText:setText("Congratulations!")
      helpText2:setText("Let's get cracking")      
      helpText3:setText("Touch to continue")      
    end

    local nextText = function(e)
      if (e.phase == "ended") then
        if (page == 1) then
          helpText:setText("Touch the coloured")
          helpText2:setText("squares to hit thugs")      
          page = 2
        elseif (page == 2) then
          helpText:setText("Practice! Touch")
          helpText2:setText("here to get a thug")      
          helpText3:setText("")
          page = 3
        elseif (page == 3) then
          leftGame:makeBadGuy("right")
          leftGame:addEventListener("hitBadGuy", hit)
        elseif (page == 4) then
          storyboard.gotoScene("difficulty", { effect = "flip", time = "300"})
        end
      end

      return true
    end

    dialogGroup:addEventListener("touch", nextText)

    group:insert(dialogGroup)

  end

  startGames()

end

function scene:key( event )
  return utils.onKeyEvent(event, storyboard.returnTo)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
  local group = self.view
  leftGame:stop()
  system.setIdleTimer( true )
  utils.stopBG()
end

function scene:didExitScene( event )
  storyboard.removeScene( "scripts.karate.howToPlay" )
  asset:disposeTemps()
  leftGame = nil
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
  local group = self.view
  asset:dispose()
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

scene:addEventListener( "didExitScene", scene )

-----------------------------------------------------------------------------------------

return scene