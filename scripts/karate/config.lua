return {
  text = {
    fill = graphics.newGradient({255, 165, 0}, {255, 255, 255}),
    font = "Impact"
  },
  game = {
    initialScore = 0,
    maxScore = 60,
    delays = {
      advance = 2000,
      resetAfterHit = 1000,
      minTimeBetweenBaddies = 0.985
    },
    randomseed = 857639
  }
}