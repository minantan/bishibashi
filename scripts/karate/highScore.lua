-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local config = require( "scripts.karate.config" )
local scene = storyboard.newScene()
local utils = require("scripts.utils")

-- include Corona's "widget" library
local widget = require "widget"
local text = require("scripts.text")
local assets = require("scripts.assets")
local scores = require("scripts.karate.scores")
--------------------------------------------

local screenW, screenH, halfW, halfH = 
  display.contentWidth, 
  display.contentHeight, 
  display.contentWidth * 0.5, 
  display.contentHeight * 0.5

local asset = assets.create()

local goToMenu = function()
  storyboard.gotoScene("menu", { effect = "flip", time = 300 })
end

function scene:createScene( event )       
  local group = self.view
  asset:loadSound("loop", "sounds/loop.wav")

  local background = asset:loadDisplay("bg", display.newImageRect( "logo.png", display.contentWidth, display.contentHeight + 100 ))
  background:setReferencePoint( display.TopLeftReferencePoint )
  background.x, background.y = 0, -50

  local highScoreBorder = asset:loadDisplay("highScoreBorder", display.newRoundedRect(30, 30, screenW - 60, screenH - 60, 10))
  highScoreBorder:setFillColor(0, 0, 80, 180)

  group:insert(background)
  group:insert(highScoreBorder)

end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  local group = self.view     
  storyboard.returnTo = "menu"
  
  asset:addListener("key", scene)  

  utils.playBG(asset.sounds["loop"], { loops = -1 })
  asset:addListener("touch", goToMenu)
  local highScoreGroup = asset:loadDisplay("highScoreGroup", display.newGroup(), true)

  -- Generate in every scene
  local y = 50
  local difficulties = { 
    { name = "hard", 
      pretty = "Hard",
      color = {255, 0, 0}
    }, 
    { name = "normal", 
      pretty = "Normal",
      color = {255, 165, 0}
    },
    { name = "easy", 
      pretty = "Easy",
      color = {0, 255, 0}
    }
  }
  local headingY = 106
  for i = 1, #difficulties do
    local fill = graphics.newGradient(difficulties[i].color, {255, 255, 255})
    local highScoreHeading = text.createText(difficulties[i].pretty, config.text.font, 35, fill, 40, headingY)
    highScoreGroup:insert(highScoreHeading.group)    
    
    headingY = headingY + 107

    local score = scores:load(difficulties[i].name)
    if (score) then
      for i = 1, #score do
        if (score[i]) then
          local scoreEntryNumber = text.createText(string.format("%2d.", i), config.text.font, 25, fill, 155, y + (i * 30))
          local scoreEntry = text.createText(score[i], config.text.font, 25, fill, 190, y + (i * 30))
          highScoreGroup:insert(scoreEntryNumber.group)
          highScoreGroup:insert(scoreEntry.group)         
        end
      end
      y = y + (scores.maxScores * 30)
    end  
    y = y + 20
  end
  group:insert(highScoreGroup)

end

function scene:key( event )
  return utils.onKeyEvent(event, storyboard.returnTo)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
  local group = self.view
  asset:disposeTemps()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
  local group = self.view
  asset:dispose()
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
