local maxScores = 3
local db = require("scripts.db")

local save = function(self, difficulty, time)
  local scoreDB = db.load("karate.score")
  if (not scoreDB) then
    scoreDB = {}
  end

  local scoreTable = scoreDB[difficulty]
  if (scoreTable) then
    scoreTable[#scoreTable + 1] = time
    table.sort(scoreTable, function (a,b)
      return (a > b)
    end)
    while (#scoreTable > maxScores) do
      table.remove(scoreTable)
    end
  else
    scoreDB[difficulty] = { time }
    scoreTable = scoreDB[difficulty]
  end

  db.save(scoreDB, "karate.score")

  return (time >= scoreTable[#scoreTable])
end

local load = function(self, difficulty)
  local scoreDB = db.load("karate.score")
  if (not scoreDB) then
    scoreDB = {}
  end
  return scoreDB[difficulty]
end

return {
  save = save,
  load = load,
  maxScores = maxScores
}
