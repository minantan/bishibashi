local config = require("scripts.karate.config")
local storyboard = require( "storyboard" )
local text = require("scripts.text")
local utils = require("scripts.utils")
local assets = require("scripts.assets")
local scores = require("scripts.karate.scores")
local scene = storyboard.newScene()

--------------------------------------------
-- forward declarations and other locals

local screenW, screenH, halfW, halfH = 
  display.contentWidth, 
  display.contentHeight, 
  display.contentWidth * 0.5, 
  display.contentHeight * 0.5

local asset = assets.create()

local client
local server
local isServer, isClient
local leftGame, rightGame

local sendToClients = function(clients, msg) 
  for i = 1, #clients do
    sendToClient(clients[i], msg)
  end
end

local sendToClient = function(client, msg) 
  if (client) then
    client:send(msg)
  end
end


local function makeServer()
  server = require("scripts.lib.Server")
  server:setCustomBroadcast("1 Player")
  server:start()
  isServer = true
end

local function makeClient() 
  client = require("scripts.lib.Client")
  client:start() 
  client:autoConnect() 
  isClient = true 
end 

-- Called when the scene's view does not exist:
function scene:createScene( event )
  local group = self.view

  -- preload sounds
  asset:loadSound("loop", "sounds/loop2.wav")
  asset:loadSound("end", "sounds/end.wav")
  asset:loadSound("explode", "sounds/explode.wav")
  asset:loadSound("cheering", "sounds/cheering.wav")
  asset:loadSound("fail", "sounds/fail.wav")

  -- preload display objects
  local waitGroup = asset:loadDisplay("waitGroup", display.newGroup())
  local waitingText = text.createText("Waiting for", config.text.font, 60, config.text.fill, halfW - 10, -150)
  local waitingText2 = text.createText("Connection!", config.text.font, 60, config.text.fill, halfW - 15, -100)
  waitingText:center()
  waitingText2:center()

  waitGroup:insert(waitingText.group)
  waitGroup:insert(waitingText2.group)

  local overText = asset:loadDisplay("overText", text.createText("Over!", config.text.font, 100, config.text.fill, halfW, halfH - 70))
  overText:center()
  overText.group.isVisible = false

  if (event.params.start == "single") then
    waitGroup.isVisible = false
  end

  group:insert(waitGroup)
  
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  local numPlayers = 1
  local clients = {}
  local group = self.view
  local karate = require("scripts.karate.karate")
  local karatePlayer = require("scripts.karate.karatePlayer")
  
  local goToMenu = function()
    storyboard.gotoScene("menu", { effect = "flip", time = 300 })
  end

  local maxScore = config.game.maxScore
  local minTimeBetweenBaddies = config.game.delays.minTimeBetweenBaddies
  if (event.params.difficulty == "hard") then
    minTimeBetweenBaddies = 0.95
  elseif(event.params.difficulty == "easy") then
    minTimeBetweenBaddies = 0.998
  end

  if (event.params.start ~= "single") then
    local waitGroup = asset.displays["waitGroup"]
    waitGroup.isVisible = true
    transition.to(asset.displays["waitGroup"], { 
      time = 200, 
      y = halfH + 70
    })
  end

  local successful = function()
    local overText = asset.displays["overText"]
    overText.group.isVisible = true
    group:insert(overText.group)

    utils.stopBG()

    local success = asset:loadDisplay("success", text.createText("Success!", config.text.font, 80, config.text.fill, halfW, - 200))
    success:center()
    group:insert(success.group)
    timer.performWithDelay(500, function()
      utils.playFX(asset.sounds["end"], { onComplete = function()
        utils.playFX(asset.sounds["explode"], { onComplete = function()
          utils.playFX(asset.sounds["cheering"])
        end})
        overText.group.isVisible = false
        transition.to(success.group, { y = halfH - 60, time = 100, onComplete = function()
          if (rightGame) then
            rightGame:gotHit()
          end
          if (event.params.start == "single") then
            local added = scores:save(event.params.difficulty, leftGame:score())
            if (added) then
              local newHighScore = asset:loadDisplay("newHighScore", text.createText("New high score!", config.text.font, 30, config.text.fill, 80, halfH + 20))
              newHighScore:center()
              group:insert(newHighScore.group)
            else
              local newHighScore = asset:loadDisplay("newHighScore", text.createText("But no high score :/", config.text.font, 30, config.text.fill, 80, halfH + 20))
              newHighScore:center()
              group:insert(newHighScore.group)
            end
          end
          asset:addListener("touch", goToMenu)
        end})
      end})
    end)
  end

  local failure = function(e)
    local overText = asset.displays["overText"]
    overText.group.isVisible = true
    group:insert(overText.group)      

    utils.stopBG()
    local fail = asset:loadDisplay("fail", text.createText("Fail!", config.text.font, 100, config.text.fill, halfW, - 200))
    fail:center()
    group:insert(fail.group)
    timer.performWithDelay(500, function()
      utils.playFX(asset.sounds["end"], { onComplete = function()
        utils.playFX(asset.sounds["explode"], { onComplete = function()
          utils.playFX(asset.sounds["fail"])
        end})
        overText.group.isVisible = false
        transition.to(fail.group, { y = halfH - 60, time = 100, onComplete = function()
          leftGame:gotHit()
          asset:addListener("touch", goToMenu)        
        end})        
      end})
    end)
  end

  local opponentScore = nil
  local startGames = function()     
    system.setIdleTimer( false )

    local leftGroup, rightGroup = 
      asset:loadDisplay("left", display.newGroup()), 
      asset:loadDisplay("right", display.newGroup())

    rightGroup.x = display.contentWidth / 2
    group:insert(leftGroup)
    group:insert(rightGroup)

    utils.stopBG()
    utils.playBG(asset.sounds["loop"], { loops = -1, fadeIn = 1000 })
    if (event.params.start ~= "single") then
      leftGroup.xScale = 0.5
      rightGroup.xScale = 0.5
      rightGame = karatePlayer.start(rightGroup, maxScore)
      if (isServer) then
        client = clients[1]
      end
    end

    leftGame = karate.start(leftGroup, maxScore, minTimeBetweenBaddies)


    local secondsLeft = 30
    local secondsLeftText = asset:loadDisplay("secondsLeft", text.createText(secondsLeft, config.text.font, 30, config.text.fill, screenW - 40, 10))
    timer.performWithDelay(1000, function(e)
      secondsLeft = secondsLeft - 1
      secondsLeftText:setText(secondsLeft)
    end, 30)

    timer.performWithDelay( 30000, function (e)
      if (leftGame) then
        leftGame:finish()        
      end
      if (event.params.start == "single") then
        local difficulty, score = event.params.difficulty, leftGame:score()
        if ((difficulty == "hard" and score > 34) or
            (difficulty == "normal" and score > 29) or
            (difficulty == "easy" and score > 29)) then
          successful()
        else
          failure()
        end
      end

      if (opponentScore) then
        if(leftGame:score() > opponentScore) then
          successful()
        else
          failure()
        end
      end
    end)


    leftGame:addEventListener("red", function(e)
      local msg = { "cmd", "red" }
      sendToClient(client, msg)
    end)
    leftGame:addEventListener("green", function(e)
      local msg = { "cmd", "green" }
      sendToClient(client, msg)
    end)
    leftGame:addEventListener("yellow", function(e)
      local msg = { "cmd", "yellow" }
      sendToClient(client, msg)
    end)

    leftGame:addEventListener("badGuy", function(e)
      local msg = { "badGuy", e.badGuyType }
      sendToClient(client, msg)
    end)
    leftGame:addEventListener("score", function(e)
      local msg = { "score", e.score }
      sendToClient(client, msg)      
    end)
    leftGame:addEventListener("endScore", function(e)
      local msg = { "endScore", e.endScore }
      sendToClient(client, msg)
    end)
  end

  local disconnected = function()
    if (leftGame) then
      leftGame:stop()
    end
    if (rightGame) then
      rightGame:stop()
    end

    local overText = asset.displays["overText"]
    overText.group.isVisible = true
    group:insert(overText.group)      

    utils.stopBG()
    local fail = asset:loadDisplay("disconnected", text.createText("Disconnected!", config.text.font, 50, config.text.fill, halfW, - 200))
    fail:center()
    group:insert(fail.group)
    if (asset.displays["success"]) then
      asset.displays["success"].group.isVisible = false
    elseif (asset.displays["fail"]) then
      asset.displays["fail"].group.isVisible = false
    elseif (waitGroup) then
      waitGroup.isVisible = false
    end
    timer.performWithDelay(500, function()
      utils.playFX(asset.sounds["end"], { onComplete = function()
        utils.playFX(asset.sounds["explode"])
        overText.group.isVisible = false
        transition.to(fail.group, { y = halfH - 40, time = 100, onComplete = function()
          asset:addListener("touch", goToMenu)        
        end })
      end})
    end)
  end


  local receiveHandler = function(event)
    local message = event.message
    local messageType, messageBody = message[1], message[2]
    if (messageType == "cmd" and rightGame) then
      if (messageBody == "red") then
        rightGame:red()
      elseif (messageBody) == "yellow" then
        rightGame:yellow()
      elseif (messageBody) == "green" then
        rightGame:green()
      end
    elseif (messageType == "badGuy" and rightGame) then
      rightGame:makeBadGuy(messageBody)
    elseif (messageType == "score" and rightGame) then
      rightGame:setScore(messageBody)
    elseif (messageType == "endScore") then
      opponentScore = messageBody
      if (leftGame:finished()) then
        if(leftGame:score() > opponentScore) then
          successful()
        else
          failure()
        end        
      end
    end
  end
  
  local countDown = function()

    local numThugs = "the most"
    if (event.params.start == "single" and "hard" == event.params.difficulty) then
      numThugs = 35
    elseif (event.params.start == "single" and ("normal" == event.params.difficulty or "easy" == event.params.difficulty)) then
      numThugs = 30
    end

    local countDownGroup = asset:loadDisplay("countDownGroup", display.newGroup())
    local countDownText = text.createText("Defeat " .. numThugs, config.text.font, 30, config.text.fill, halfW, - 200)
    local countDownText2 = text.createText("thugs in 30 seconds!", config.text.font, 30, config.text.fill, halfW, - 160)
    local countDownText3 = text.createText("3", config.text.font, 30, config.text.fill, halfW, - 100)
    countDownText:center()
    countDownText2:center()
    countDownText3:center()
    local waitGroup = asset.displays["waitGroup"]
    waitGroup.isVisible = false

    asset:addTimer(timer.performWithDelay(1000, function()
      countDownText3:setText("2")
    end))
    asset:addTimer(timer.performWithDelay(2000, function()
      countDownText3:setText("1")
    end))
    asset:addTimer(timer.performWithDelay(3000, function()
      countDownText3:setText("Start!")
    end))
    asset:addTimer(timer.performWithDelay(3500, function()
      startGames()
    end))

    countDownGroup:insert(countDownText.group)
    countDownGroup:insert(countDownText2.group)
    countDownGroup:insert(countDownText3.group)
    group:insert(countDownGroup)

    transition.to(countDownGroup, { y = halfH, time = 300 })

  end


  if (event.params.start == "server") then
    makeServer()
    local addPlayer = function(event)
      local client = event.client
      local index = 1
      while(clients[index]) do
        index = index+1
      end
      clients[index] = client
      client:sendPriority({1,playerNumber = index})
      numPlayers = numPlayers+1
      server:setCustomBroadcast(numPlayers.." Players")
      countDown()
    end
    asset:addListener("autolanPlayerJoined", addPlayer)
    asset:addListener("autolanReceived", receiveHandler)
    asset:addListener("autolanPlayerDropped", disconnected)
  elseif (event.params.start == "client") then
    makeClient()
    local numberOfServers = 0
    asset:addListener("autolanConnected", countDown)
    asset:addListener("autolanReceived", receiveHandler)
    asset:addListener("autolanDisconnected", disconnected)  
  elseif (event.params.start == "single") then
    isServer = true
    countDown()
  end

  if (event.params.start == "single") then
    storyboard.returnTo = "difficulty"
  else
    storyboard.returnTo = "menu"
  end

  asset:addListener("key", scene)

end

function scene:key( event )  
  return utils.onKeyEvent(event, storyboard.returnTo)
end

function scene:exitScene( event )
  local group = self.view
  if (leftGame) then
    leftGame:stop()
  end
  system.setIdleTimer( true )
end

function scene:didExitScene( event )
  storyboard.removeScene( "scripts.karate.karateScene" )
  asset:disposeTemps()
  if (isServer and server ~= nil) then
    server:disconnect()
    server:stop()
    server = nil
  elseif (isClient) then
    client:disconnect()
    client:stop()
    client = nil
  end
  clients = nil
  client = nil
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
  local group = self.view
  leftGame,rightGame = nil,nil
  asset:dispose()
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

scene:addEventListener( "didExitScene", scene )

-----------------------------------------------------------------------------------------

return scene