local sprites = require("scripts.karate.sprites")
local utils = require("scripts.utils")

local gotHitSound,leftSound,centerSound,rightSound = audio.loadSound("sounds/error.wav"),
  audio.loadSound('sounds/center.wav'),
  audio.loadSound('sounds/left.wav'),
  audio.loadSound('sounds/right.wav')

local idle, punch, lKick, rPunch, hit = sprites.makeSequence("idling", "goodGuy", 500, 0),
  sprites.makeSequence("punching", "goodGuyPunch", 250, 1),
  sprites.makeSequence("left kicking", "goodGuyLKick", 300, 1),
  sprites.makeSequence("right punching", "goodGuyRPunch", 300, 1),
  sprites.makeSequence("got hit", "goodGuyHit", 500, 0)

local makeGoodGuy = function(x, y, scaleX, scaleY, soundOn) 
  local goodGuySprite = display.newSprite(sprites.sheet, {
    idle,
    punch,
    lKick,
    rPunch,
    hit
  })

  goodGuySprite.x, goodGuySprite.y = x, y
  goodGuySprite.xScale, goodGuySprite.yScale = scaleX, scaleY

  local hitIfAny = function(direction) 
    goodGuySprite:dispatchEvent({ 
      name = "hitBadGuy",
      direction = direction,
      target = goodGuySprite
    })
  end

  local goodGuy = {
    idle = function(self)
      goodGuySprite:setSequence("idling")
      goodGuySprite:play()
    end,
    punch = function(self)
      if (goodGuySprite.sequence ~= "punching") then
        if (soundOn) then
          utils.stopFX()
          utils.playFX(centerSound)
        end
        goodGuySprite:setSequence("punching")
      end
      goodGuySprite:play()
      hitIfAny("centre")
    end,
    lKick = function(self)
  
      if (goodGuySprite.sequence ~= "left kicking") then
        if (soundOn) then
          utils.stopFX()
          utils.playFX(leftSound)
        end
        goodGuySprite:setSequence("left kicking")
      end
      goodGuySprite:play()
      hitIfAny("left")
    end,
    rPunch = function(self)
      if (goodGuySprite.sequence ~= "right punching") then
        if (soundOn) then
          utils.stopFX()
          utils.playFX(rightSound)
        end
        goodGuySprite:setSequence("right punching")
      end
      goodGuySprite:play()
      hitIfAny("right")
    end,
    gotHit = function(self)
      if (soundOn) then
        utils.stopFX()
        utils.playFX(gotHitSound)
      end
      if (goodGuySprite.sequence ~= "got hit") then
        goodGuySprite:setSequence("got hit")
      end
      goodGuySprite:play()
    end,
    sprite = goodGuySprite,
    removeSelf = function(self)
      goodGuySprite:removeSelf()
    end,
    addEventListener = function(self, eventName, listener)
      goodGuySprite:addEventListener(eventName, listener)
    end
  }

  goodGuySprite:addEventListener("sprite", function(e)
    if (e.phase == "ended") then
      goodGuy:idle()
    end
  end)

  return goodGuy
end

return {
  makeGoodGuy = makeGoodGuy
}