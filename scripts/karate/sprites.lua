local sprites = require('scripts.sprites')

local framePairs = {
  bg = {
    x = 639, 
    y = 7, 
    width = 159, 
    height = 237,
  },
  goodGuy = {
    startX = 0,
    startY = 4,
    width = 62,
    height = 101,
    total = 2,
  },
  goodGuyPunch = {
    startX = 69,
    startY = 4,
    width = 68,
    height = 100,
    total = 4
  },
  goodGuyLKick = {
    startX = 137,
    startY = 3,
    width = 97,
    height = 97,
    total = 3
  },
  goodGuyRPunch = {
    startX = 245,
    startY = 3,
    width = 81,
    height = 97,
    total = 3
  },
  goodGuyHit = {
    startX = 328,
    startY = 4,
    width = 76,
    height = 103,
    total = 2
  },
  badGuyLeft = {
    startX = 422,
    startY = 10,
    width = 63,
    height = 96,
    total = 3  
  },
  badGuyHitLeft = {
    startX = 550,
    startY = 7,
    width = 83, 
    height = 60,
    total = 1
  },
  badGuyHitCentre = {
    startX = 550,
    startY = 124,
    width = 83, 
    height = 60,
    total = 1
  }

}

local spriteSheet = graphics.newImageSheet( "images/karate/karate.png", sprites.constructFrameOptions(framePairs))

return {
  sheet = spriteSheet,
  frames = framePairs,
  makeSequence = function(sequenceName, spriteName, time, loopCount)
    return sprites.makeSequence(framePairs, sequenceName, spriteName, time, loopCount)
  end  
}
