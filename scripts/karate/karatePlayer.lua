-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------
local config = require("scripts.karate.config")

local sprites = require("scripts.karate.sprites")
local text = require("scripts.text")
local status = require("scripts.status")
local goodGuy = require("scripts.karate.goodGuy")
local badGuys = require("scripts.karate.badGuy")

--------------------------------------------
-- forward declarations and other locals
local screenW, screenH, halfW, halfH = 
	display.contentWidth, 
	display.contentHeight, 
	display.contentWidth * 0.5, 
	display.contentHeight * 0.5


-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------



-- Called when the scene's view does not exist:
local start = function( group, maxScore, options)

	require("scripts.lib.randomlua")
	options = options or {}
	local random = mwc(config.game.randomseed)

	local lastBadGuy = 0
	local finished = false

	local statusBar = status.createStatus(config, "Them", screenH - 25, maxScore)


	-- create a grey rectangle as the backdrop
	local background = display.newImageRect(sprites.sheet, sprites.frames["bg"].start, screenW, screenH + 50)
	background.x, background.y = halfW, halfH

	-- bad guy
	local badGuy = badGuys.generateBadGuys()
	-- good guy
	local gg = goodGuy.makeGoodGuy(halfW, halfH - 150, 2, 2 * group.xScale, options.sound)
	gg:idle()

  local k = display.newGroup()

	gg:addEventListener("hitBadGuy", function (e)
		for i = 1, #badGuy.badGuys() do
			local thisBadGuy = badGuy.badGuys()[i]
      if (thisBadGuy.sprite.y <= (e.target.y + 100)) then
        if (thisBadGuy.badGuyType == e.direction) then
        	k:dispatchEvent({ name = "hitBadGuy" })
          thisBadGuy:gotHit()
          return
        end
      end
    end
	end)

	local buttonGroup = display.newGroup()
	local badGuyGroup = display.newGroup()

	group:insert( background )
	group:insert( gg.sprite )
	group:insert( badGuyGroup )	

	local screenFlash = display.newRect(0, 0, screenW, screenH + 50)
	screenFlash:setFillColor(255, 255, 255)
	screenFlash.alpha = 0
	screenFlash.x, screenFlash.y = halfW, halfH		
	group:insert (screenFlash)

	if (options.buttons) then
		local yellow, red, green = display.newRect(0, 0, screenW / 3, screenW / 3),
		display.newRect(0, 0, screenW / 3, screenW / 3),
		display.newRect(0, 0, screenW / 3, screenW / 3)

	  yellow:setFillColor(255, 255, 0, 80)
	  red:setFillColor(255, 0, 0, 80)
	  green:setFillColor(0, 255, 0, 80)

		yellow.x, red.x, green.x = screenW * 0.17, screenW * 0.5, screenW * 0.83
		yellow.y, red.y, green.y = halfH + 173, halfH + 173, halfH + 173

		red:addEventListener("touch", function(e)
      if (not badGuy.isStopped()) then
				gg.punch()
			end
    end)

    yellow:addEventListener("touch", function(e)
      if (not badGuy.isStopped()) then
				gg.lKick()
			end
    end)

    green:addEventListener("touch", function(e)
      if (not badGuy.isStopped()) then
				gg.rPunch()
			end
    end)  

		buttonGroup:insert( yellow )
		buttonGroup:insert( red )
		buttonGroup:insert( green )

  end
	buttonGroup:insert( statusBar.group )
	group:insert( buttonGroup )

	local mainEventLoop = function(e)
		if (finished) then
			return
		end

		if (not badGuy.isStopped()) then
			for i = 1, #badGuy.badGuys() do
				local badGuyY = badGuy.badGuys()[i].sprite.y
  			if (badGuyY ~= nil and gg.sprite.y and (badGuyY <= gg.sprite.y + 50)) then
					badGuy.stopBadGuys()					
					gg:gotHit()
					timer.performWithDelay(config.game.delays.resetAfterHit, function(e)						
						badGuy.clear()
						gg:idle()
						badGuy.startBadGuys()
					end)
					break
				end
			end
		end
	end
  Runtime:addEventListener("enterFrame", mainEventLoop)

  return {
  	red = function(self) 
	 		if (not badGuy.isStopped()) then
				gg:punch()
			end
	 	end,
		yellow = function(self)
			if (not badGuy.isStopped()) then
				gg:lKick()
			end
		end,
		green = function(self)
			if (not badGuy.isStopped()) then
				gg:rPunch()
			end
		end,
		makeBadGuy = function(self, badGuyType)
			if (not badGuy.isStopped()) then
				local bg = badGuy.generateBadGuy(2, 2 * group.xScale, badGuyType)
				badGuyGroup:insert(bg.sprite)
				bg:advance(gg.sprite.y, config.game.delays.advance)
				bg:addEventListener("gotHit", function(e)
	        transition.to(screenFlash, {
	          alpha = 1,
	          time = 100,
	          onComplete = function ( e)    
	            transition.to(screenFlash, {
	              alpha = 0,
	              time = 100
	            })
	          end
	        })
				end)
			end
		end,
		setScore = function(self, score)
			statusBar:setScore(score)
		end,
		stop = function(self)
			badGuy.stopBadGuys()
			badGuy.clear()
			Runtime:removeEventListener("enterFrame", mainEventLoop)		
		end,
		gotHit = function(self)
			gg:gotHit()
		end,
		addEventListener = function(self, event, listener) 
			k:addEventListener(event, listener)			
		end
	}

end

return {
	start = start,
}