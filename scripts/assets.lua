local create = function()
    return {
      sounds = {},
      displays = {},
      displayTemps = {},
      runtimeListeners = {},
      timers = {},
      addListener = function(self, event, listener)
        local listeners = self.runtimeListeners[event]
        if (not listeners) then
          self.runtimeListeners[event] = { listener }
        else
          self.runtimeListeners[event][#listeners + 1] = listener
        end
        Runtime:addEventListener(event, listener)
      end,
      loadSound = function(self, handle, path)
        local sound = audio.loadSound(path)
        self.sounds[handle] = sound
        return sound
      end,
      loadDisplay = function(self, handle, object, temp)
        if (temp) then
          self.displayTemps[handle] = object
        else
          self.displays[handle] = object
        end
        return object
      end,
      addTimer = function(self, handle, timingObject)
        self.timers[handle] = timingObject
        return timingObject
      end,
      disposeTemps = function(self)
        for k, v in pairs(self.runtimeListeners) do
          for i = 1,#v do
            Runtime:removeEventListener(k, v[i])
            v[i] = nil
          end
          self.runtimeListeners[k] = nil
        end
        for k, v in pairs(self.timers) do
          timer.cancel(v)
          self.timers[k] = nil
        end
        for k, v in pairs(self.displayTemps) do
          v:removeSelf()
          self.displayTemps[k] = nil
        end
        self.runtimeListeners = {}
        self.timers = {}
      end,
      dispose = function(self)
        self:disposeTemps()      
        for k, v in pairs(self.sounds) do
          audio.dispose(v)
          self.sounds[k] = nil
        end
        for k, v in pairs(self.displays) do
          v:removeSelf()
          self.displays[k] = nil
        end
        self.sounds = {}
        self.displays = {}
      end
    }
  end
return {
  create = create
}