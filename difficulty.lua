-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local config = require( "scripts.karate.config" )
local scene = storyboard.newScene()
local utils = require("scripts.utils")
local text = require("scripts.text")
local assets = require("scripts.assets")
local widget = require("scripts.customWidget")
--------------------------------------------

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:

local asset = assets.create()

function scene:createScene( event )	
	local group = self.view
	asset:loadSound("selected", "sounds/selected.wav")
	asset:loadSound("loop", "sounds/loop.wav")

	-- display a background image
	local background = display.newImageRect( "logo.png", display.contentWidth, display.contentHeight + 100 )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, -50


	local buttonGroup = asset:loadDisplay("buttons", display.newGroup())

	local buttonWidth, buttonHeight, buttonRadius = display.contentWidth - 120, 40, 10

	local howToPlay = widget.newButton(0, 310, buttonWidth, buttonHeight, buttonRadius, { fill = {0,0,255,150} }, "How to Play")
	utils.center(howToPlay)
	local easy = widget.newButton(0, 360, buttonWidth, buttonHeight, buttonRadius, { fill = {0,255,0,150} }, "Easy")
	utils.center(easy)
	local normal = widget.newButton(0, 410, buttonWidth, buttonHeight, buttonRadius, { fill = {255,255,0,150} }, "Normal")
	utils.center(normal)
	local hard = widget.newButton(0, 460, buttonWidth, buttonHeight, buttonRadius, { fill = {255,0,0,150} }, "Hard")
	utils.center(hard)

	buttonGroup:insert( howToPlay )
	buttonGroup:insert( easy )
	buttonGroup:insert( normal )
	buttonGroup:insert( hard )

	group:insert( background )
	group:insert(buttonGroup)

	howToPlay:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.howToPlay", { effect = "flip", time = 300 })
			end
		})
		return true	
	end)

	easy:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.karateScene", { effect = "fade", time = 500, params = { start = "single", difficulty = "easy" } })
			end
		})
		return true	

	end)
	normal:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.karateScene", { effect = "fade", time = 500, params = { start = "single", difficulty = "normal" } })
			end
		})		
		return true	
	end)
	hard:addEventListener( "touch",  function(e)
		utils.playFX(asset.sounds["selected"], {
			onComplete = function()
				storyboard.gotoScene( "scripts.karate.karateScene", { effect = "fade", time = 500, params = { start = "single", difficulty = "hard" } })
			end
		})				
		return true	
	end)

end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view	
	storyboard.returnTo = "menu"

  utils.playBG(asset.sounds["loop"], { loops = -1 })
	asset:addListener("touch", function()
		storyboard.gotoScene("menu", { effect = "flip", time = 300 })
	end)
	asset:addListener("key", scene)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	-- audio.fadeOut({ channel = 1, time = 500})
	utils.stopBG()
	asset:disposeTemps()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	asset:dispose()
end

function scene:key( event )
	return utils.onKeyEvent(event, storyboard.returnTo)
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene